<?php get_header(); ?>

<div id="loop">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="grid_16 alpha">
			<h2 class="grid_12 alpha">
				<a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
			</h2>
			<time>
				<?php the_time('F j, Y'); ?>
			</time>
			<div class="post_permalink">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">Permalink</a>
			</div>
			<div class="content grid_16 alpha">
				<img class="item_image" src="<?php the_field('item_image'); ?>" alt="<?php the_title(); ?>" />
				<?php the_content(__('Read more'));?>
			</div>
			<div class="meta grid_16 alpha">
				<div class="via grid_16 alpha">
					Via <a href="<?php the_field('via_link_url'); ?>"><?php the_field('via_link_name'); ?></a>
				</div>
				<div class="get grid_5 prefix_1 alpha">
					Get it ($<?php the_field('price'); ?>) <a href="#">Get it link</a>
				</div>
				<div class="share grid_4 prefix_6 omega">
					Share it <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>" class="facebook" title="Share with friends on Facebbok">Like</a> <a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&amp;url=<?php echo urlencode(get_permalink($post->ID));?>&amp;via=morewings&amp;related=morewings" class="twitter fancybox.iframe" title="Tweet this item">Tweet</a>
				</div>
			</div>
		</article>
	<?php endwhile; else: ?>
  <p><strong>There has been a glitch in the Matrix.</strong><br />
  There is nothing to see here.</p>
  <p>Please try somewhere else.</p>
 <?php endif; ?>

 <div class="postnavigation">
  <p><?php next_posts_link('&laquo; Older Entries') ?><?php previous_posts_link(' | Newer Entries &raquo;') ?></p>
 </div>
</div> <!-- end #loop -->

<?php get_footer(); ?>