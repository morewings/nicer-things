<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!--[if lt IE 10]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
	<title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?> <?php if ( !wp_title('', true, 'left') ); { ?> | <?php bloginfo('description'); ?> <?php } ?></title>
	<link rel="stylesheet/less" href="<?php bloginfo( 'template_directory' ); ?>/css/reset.less">
	<link rel="stylesheet/less" href="<?php bloginfo( 'template_directory' ); ?>/css/960.less">
	<link rel="stylesheet/less" href="<?php bloginfo( 'template_directory' ); ?>/css/text.less">
	<link rel="stylesheet/less" href="<?php bloginfo( 'template_directory' ); ?>/css/style.less">
	<link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/tipsy.css">
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/less-1.1.5.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.7.1.min.js">\x3C/script>')</script>
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery.tipsy.js"></script>
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/social_popup.js"></script>
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/application.js"></script>
	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class($class); ?>>

<div class="container_16">
	<header class="grid_10 prefix_3">
		<h1>
			<a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('description'); ?>">
				<?php bloginfo('name'); ?>
			</a>
		</h1>
	</header>
	<div class="clear"></div>