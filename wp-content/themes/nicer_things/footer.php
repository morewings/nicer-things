</div><!-- end .container_16 -->
<footer>
	<div class="container_16">
		<div class="logo grid_2 push_7">Logo</div>
		<div class="info grid_16">
			<span class="small_logo">Nicer Things</span> by <span class="author">Sebastian de With</span>
		</div>
		<div class="note grid_16">
			I may occasionally earn a few bucks if you buy Nicer Things through my website. Your support is appreciated!
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>